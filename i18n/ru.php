<?php defined('SYSPATH') OR die('No direct script access.');
return array(

    /* CONTACT */
    'Contacts' => 'Контакты',
    'Contact form' => 'Обратная связь',

    'Ad number' => 'Номер объявления',
    'Your e-mail' => 'Ваш e-mail',
    'Message text' => 'Текст сообщения',
    'Enter captcha code' => 'Укажите код с картинки',
    'Send message' => 'Отправить сообщение',

    'Message from user' => 'Сообщение от пользователя',
    'Your message successfully sended' => 'Ваше сообщение успешно отправлено',

    '' => '',
);