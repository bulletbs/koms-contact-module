<?php defined('SYSPATH') or die('No direct script access.');

if(!Route::cache()){
    $_init_actions = array(
        'index',
        'send',
    );
    Route::set('contact', 'contact(/<action>)', array('action' => '('.implode('|', $_init_actions).')'))
        ->defaults(array(
            'controller' => 'contact',
            'action' => 'index',
        ));
}