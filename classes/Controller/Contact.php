<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * KoMS Contact module controller
 */
class Controller_Contact extends Controller_System_Page
{
    public function before(){
        parent::before();

        /* Script & style */
        $this->styles[] = "media/libs/pure-release-0.6.0/forms.css";

        if($this->auto_render){
            /* Bradcrumbs */
            $this->breadcrumbs->add(__('Контактная информация'), Route::get('contact')->uri());
        }
    }

    /**
     * Главная страница
     */
    public function action_index(){
        /* Setup module config */
        $cfg = Kohana::$config->load('contact')->as_array();
        if(!empty($cfg['title']))
            $this->title = $cfg['title'];
        if(!empty($cfg['description']))
            $this->description = $cfg['description'];
        if(!empty($cfg['keywords']))
            $this->keywords = $cfg['keywords'];

        /* Process posted data */
        $errors = array();
        if (HTTP_Request::POST == $this->request->method()){
            $post = Arr::extract($_POST, array(
                'text',
                'email',
                'number',
                'captcha',
            ));
            $validation = Validation::factory($post)
                ->rule('email', 'not_empty')
                ->rule('email', 'email', array(':value'))
                ->rule('text', 'not_empty')
                ->rule('text', 'min_length', array(':value',10))
                ->rule('text', 'max_length', array(':value',1000))
                ->labels(array(
                    'email' => __('Your e-mail'),
                    'text' => __('Message text'),
                    'captcha' => __('Enter captcha code'),
                ))
            ;
            if(!$this->logged_in)
                $validation->rules('g-recaptcha-response', array(
                    array('Captcha::check', array(':value', ':validation', ':field'))
                ));

            if($validation->check()){
                Email::instance()
                    ->to($this->config['contact_email'])
                    ->from($post['email'])
                    ->subject($this->config['project']['name'] .': '.__('Message from user'))
                    ->message(View::factory('contact/letter', array(
                        'email'=> $post['email'],
                        'number'=> $post['number'],
                        'text'=> strip_tags($post['text']),
                    ))->render()
                        , true)
                    ->send();
                Flash::success(__("Your message successfully sended"));
                $this->template->content = $this->getContentTemplate('contact/success');
                return;
            }
            else
                $errors = $validation->errors('error/validation');
        }

        $user = $this->logged_in ? $this->current_user : ORM::factory('User');
        $this->template->content->set(array(
            'user' => $user,
            'logged' => $this->logged_in,
            'errors' => $errors,
        ));
    }
}