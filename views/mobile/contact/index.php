<?php defined('SYSPATH') OR die('No direct script access.');?>

<h1 class="uk-h2"><?php echo __('Contact form')?></h1>

<?=Form::open('', array('class' => 'uk-form  uk-form-stacked', 'enctype' => 'multipart/form-data','id'=>'addForm'))?>
    <?if(isset($errors)) echo View::factory('mobile/error/validation', array('errors'=>$errors))->render() ?>
    <fieldset>
        <div class="uk-form-row">
            <?= Form::label('name', __('Message text'), array('class'=>'uk-form-label'))?>
            <?= Form::textarea('text', Arr::get($_POST,'text' ) , array('class'=>isset($errors['name']) ? 'error-input': '')) ?>
        </div>
        <div class="uk-form-row"><?= Form::label('email', 'E-mail', array('class'=>'uk-form-label'))?>
            <?= Form::input('email', Arr::get($_POST,'email' , $logged ? $user->email : NULL) , array('class'=>isset($errors['name']) ? 'error-input': '')) ?>
        </div>
        <div class="uk-form-row">
            <?= Form::label('number', __('Ad number'), array('class'=>'uk-form-label'))?>
            <?= Form::input('number', Arr::get($_POST, 'number') , array('class'=>isset($errors['name']) ? 'error-input': '')) ?>
        </div>
        <?if(!Auth::instance()->logged_in()):?>
        <div class="uk-form-row">
            <?php echo Captcha::instance(); ?>
        </div>
        <?endif?>
        <div class="uk-form-row">
            <?=Form::button('update', __('Send message'), array('class' => 'uk-button uk-button-primary'));  ?>
        </div>
    </fieldset>
<?=Form::close()?>
