<?php defined('SYSPATH') OR die('No direct script access.');?>

<h1><?php echo __('Contact form')?></h1>

<?=Form::open('', array('class' => 'pure-form  pure-form-stacked', 'enctype' => 'multipart/form-data','id'=>'addForm'))?>
    <?if(isset($errors)) echo View::factory('error/validation', array('errors'=>$errors))->render() ?>
    <fieldset>
        <?= Form::label('name', __('Message text'))?>
        <?= Form::textarea('text', Arr::get($_POST,'text' ) , array('class'=>isset($errors['name']) ? 'error-input': '')) ?>

        <?= Form::label('email', 'E-mail')?>
        <?= Form::input('email', Arr::get($_POST,'email' , $logged ? $user->email : NULL) , array('class'=>isset($errors['name']) ? 'error-input': '')) ?>

        <?= Form::label('number', __('Ad number'))?>
        <?= Form::input('number', Arr::get($_POST, 'number') , array('class'=>isset($errors['name']) ? 'error-input': '')) ?>

        <?if(!Auth::instance()->logged_in()):?>
            <?php echo Captcha::instance(); ?>
        <?endif?>
        <br>
        <?=Form::submit('update', __('Send message'), array('class' => 'pure-button pure-button-primary'));  ?>
    </fieldset>
<?=Form::close()?>
